# PABOLB

The PlayAudio12 Big Obvious Light Bar

## Stuff you will need to build the circuit

- Some WS2812B LED strip AKA Neopixel strip
- A microcontroller with at least 3 GPIO - I used a ESP8266 Wemos D1 clone (about $2)
- A 1/4" TRS plug
- A 5V power supply. 12 LEDs draw about 500mA at full white, so 5V/1A is a good margin
- The necessary wire, cable etc
- soldering iron + skillz

### Optional

- Aluminium channel for mounting LED strip, maybe a diffuser for the front

## Hardware setup

![Alt text](fun.png)

Using addressable LEDs is a bit more expensive than "dumb" RGB strip, but has some nice advantages:
 - Integrated LED driver = no external components for us
 - We can run microcontroller + strip from a single 5V supply
 - Cool animated patterns

Pay close attention to the three conductors on the LED strip it may not look like the above picture! Sometimes the data pin is in the middle, sometimes it's not.

The data cable between D3 and the LED strip should be fairly short (a couple of feet). The LEDs expect a 5V data signal and the microcontroller outputs 3.3V. This is OK for short runs but if you need to go longer, you may need a level shifter. See [this discussion](https://quinled.info/2021/03/10/maximum-length-data-wire-leds-ws2812b-sk6812-ws2815/)


## Software setup (mac)

- Download and install [Arduino IDE](https://www.arduino.cc/en/software)
- Connect the board with USB and run Arduino IDE. In the serial port drop down (top left) you should see a port listed like `/dev/cu.wchusbserial12345912353` or something. If not, you probably need the USB drivers for the board. Quit, [Install this](https://www.wch.cn/download/CH341SER_MAC_ZIP.html), and load the IDE again.
- From the serial port drop down, choose "Select other board and port", then choose __LOLIN(WEMOS) D1 R2 & mini__ as the board, and `/dev/cu.wchusbserialxxxxxxxx` as the port.
- Tools -> Manage Libraries and search for "FastLED". Install "FastLED by Daniel Garcia" (RIP)
- Delete the boilerplate code in the sketch editor, copy/paste in the contents of the PABOLB.ino file, then save the sketch (cmd-s)
- Upload to the board by pressing the arrow button at the top, or cmd-u.

## Operation/info

The PA12 control outputs are "open collector" outputs, meaning that they pull the output down to ground when activated. For this, the internal pull up resistors in the ESP8266 are activated. No extra hardware is required.

There are three indicated states: 

- __Failover disarmed__ : a sweeping blue pattern to let you know there is no failover in operation. This occurs whether the unit is in Scene A or B.
- __Failover armed, Scene A__ : Solid green
- __Failover armed, Scene B__ : Solid red

## links

[iconnectivity page] (https://iconnectivity.supportbee.io/53-iconnectivity-knowledgebase/538-product-specific/10570-how-do-i-use-the-control-jacks-on-the-playaudio12)

## pics and videos

![Alt text](vid.mp4)
![Alt text](MVI_9981.mp4)
