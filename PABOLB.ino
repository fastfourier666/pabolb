
#define PIN_AB D2       // connected to Ring of TRS
#define PIN_ARM D1      // connected to Tip of TRS
#define PIN_LED D3      // connected to data in (DIN) of LED strip
#include <FastLED.h>

#define NUM_LEDS 12     // We have 12 LEDs
CRGB leds[NUM_LEDS];    // Array to store colour data for LEDs
uint32_t timer = 0;     // Timer for "standby" pattern

void setup() {
  // Enable pullup resistors on both inputs of the TRS. The PA12 will pull these lines
  // to ground to indicate a change of status
  pinMode(PIN_AB, INPUT_PULLUP);
  pinMode(PIN_ARM, INPUT_PULLUP);

  // Tell FastLED about our setup
  FastLED.addLeds<NEOPIXEL, PIN_LED>(leds, NUM_LEDS);
}

void loop() {
  if (digitalRead(PIN_ARM)) {         // is failover armed?
    if (digitalRead(PIN_AB)) {        // Failover is armed, so are we on A or B?
      slc(CRGB::Green);               // A = green
    } else {
      slc(CRGB::Red);                 // B = red
    }

  } else {                            // failover is not armed
    if (millis() - timer >= 20) {     // we update the fading pattern every 20 ms
      timer = millis();               // reset timer
      standbyPattern();               // generate the next frame of the pattern
    }
  }
  FastLED.show();                     // Update the LED strip
}

/*
  This function just sets all the LEDs to a specific colour
*/
void slc(CRGB c) {
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i] = c;
  }
}

/*
  Generate a frame of our standby pattern
*/
void standbyPattern() {
  static uint8_t pos = 0;
  static int8_t dir = 1;
  static uint32_t lastMove = 0;

  // fade all the LEDs in the strip towards black
  for (int i = 0; i < NUM_LEDS; i++) {
    leds[i].fadeToBlackBy(8);
  }

  // Every 100 ms, we light up the next LED in a back and forth pattern.
  if (millis() - lastMove >= 100) {       
    lastMove = millis();
    leds[pos] = CRGB(0, 0, 128);            // 50% blue
    pos += dir;                             // set up for the next LED
    if (pos == NUM_LEDS - 1) dir = -1;      // if we reached the last LED, go in the other direction
    if (pos == 0) dir = 1;                  // if we reached the first LED, go in the other direction
  }
}